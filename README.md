<!-- omit in toc -->
# CPQs Abertas - Hub

<!-- omit in toc -->
## índice:
- [📝 Descrição:](#-descrição)
- [🔗 Dependências e Ferramentas:](#-dependências-e-ferramentas)
- [🚀 Compilação e Execução:](#-compilação-e-execução)
- [🌱 Padrão de Contruibuição:](#-padrão-de-contruibuição)
  - [🖌️ CSS:](#️-css)
  - [📤 Commits:](#-commits)
- [📁 Organização dos Arquivos:](#-organização-dos-arquivos)
  - [🌳 Estrutura das pastas:](#-estrutura-das-pastas)
  - [📂 Descrição Pastas \& Arquivos:](#-descrição-pastas--arquivos)

## 📝 Descrição:

Este repositório armazena o código para o [HUB CPQs Abertas](http://hub-cpqs-abertas.s3-website-sa-east-1.amazonaws.com/), site de um projeto que tem como objetivo principal expor de forma quantitativa a produção científica dos institutos da Universidade de São Paulo (USP), promovendo a visibilidade e acessibilidade a essa importante atividade acadêmica.

## 🔗 Dependências e Ferramentas:

Este projeto utiliza as seguintes dependências e ferramentas:
- [Node.js](https://nodejs.org/en/): Plataforma de execução de código JavaScript do lado do servidor.
- [npm (Node Package Manager)](https://www.npmjs.com/): Gerenciador de pacotes do Node.js.
- [Vite](https://vitejs.dev/): Build tool que oferece um desenvolvimento rápido e eficiente para projetos JavaScript.
- [TypeScript](https://www.typescriptlang.org/): Linguagem de programação que adiciona tipagem estática ao JavaScript, trazendo mais segurança e facilidade na manutenção de código.
- [shadcn/ui](https://ui.shadcn.com/): Biblioteca de componentes UI para acelerar o desenvolvimento.
- [Tailwind CSS](https://tailwindcss.com/): Framework de CSS utility-first para a construção de interfaces web modernas.

## 🚀 Compilação e Execução:

Após instalação das dependências, para executar o projeto pela primeira vez, digite no terminal no repositório com arquivo `package.json`:

```bash
npm install 
```

Para rodar e ver o projeto no seu navegador:

```bash
npm run dev
```

## 🌱 Padrão de Contruibuição:

### 🖌️ CSS:  
- Usar o padrão [BEM](https://desenvolvimentoparaweb.com/css/bem/) - Block / Element / Modifier;

### 📤 Commits:
- O padrão de commits deve ser o seguinte:
```
<tipo> | <ciclo/(optional)issue>: <descricao>
```

- Exemplo: 
  - `feat | Ciclo 10 - #9: Inserindo nova funcionalidade para o carrossel de sites`;
  - [Vídeo Referência](https://www.youtube.com/watch?v=OJqUWvmf4gg);

Exemplos de tipos:
- `feat`
- `fix`;
- `docs`;
- `style`;
- `refactor`;
- `chore`;
- `revert`;
- `merge`;



## 📁 Organização dos Arquivos:

### 🌳 Estrutura das pastas:
```
.
├── node_modules/
├── public/
│   ├── icons/
│   ├── logos/
│   └── screenshots/
├── src/
│   ├── components/
│   │   └── ui/
│   ├── lib/
│   ├── pages/
│   ├── styles/
│   ├── vite-env.d.ts
│   ├── App.jsx
│   └── index.jsx
├── .eslintrc.cjs
├── .gitignore
├── components.json
├── index.html
├── package-lock.json
├── package.json
├── postcss.congig.js
├── README.md
├── tailwind.config.js
├── tsconfig.json
├── tsconfig.node.json
└── vite.config.js
```

### 📂 Descrição Pastas & Arquivos:
- **node_modules/**
  - Esta pasta contém todas as dependências do projeto que foram instaladas pelo gerenciador de pacotes (como npm ou yarn). Normalmente, você não precisa modificar ou interagir diretamente com os arquivos nesta pasta, pois ela é gerada automaticamente pelo gerenciador de pacotes.

- **public/**
  - **icons/**: Esta pasta é destinada a armazenar ícones que serão usados em sua aplicação.
  - **logos/**: Esta pasta é destinada a armazenar logos que serão usados em sua aplicação.
  - **screenshots/**: Esta pasta é destinada a armazenar capturas de tela ou imagens relacionadas ao projeto.

- **src/**
  - **components/**:
    - **ui/**: Esta subpasta contém componentes de interface do usuário que são adicionados automaticamente pelas instalações feitas pelo shadcn;
  - **lib/**: Esta pasta é destinada a armazenar bibliotecas ou utilitários comuns que podem ser compartilhados em diferentes partes do projeto.
  - **pages/**: Esta pasta contém os componentes que representam páginas individuais da aplicação.
  - **styles/**: Esta pasta é destinada a armazenar arquivos de estilo CSS que são utilizados em toda a aplicação.
  - **vite-env.d.ts**: Este arquivo é uma definição de tipo para o Vite, usado para configurar tipos globais ou estender tipos existentes para o ambiente de desenvolvimento do Vite.
  - **App.jsx**: Este é o arquivo principal do componente App, que é o ponto de entrada principal da aplicação React.
  - **index.jsx**: Este é o arquivo de entrada principal que renderiza o aplicativo React no HTML.

- **.eslintrc.cjs**: Este é o arquivo de configuração do ESLint para o projeto, utilizado para definir regras de linting para garantir a qualidade do código.

- **.gitignore**: Este arquivo especifica quais arquivos e pastas devem ser ignorados pelo Git durante o versionamento do código fonte.

- **components.json**: Este arquivo pode conter metadados ou informações sobre os componentes utilizados na aplicação.

- **index.html**: Este é o arquivo HTML principal que serve como ponto de entrada para a aplicação, e geralmente é usado para incluir o script JavaScript principal.

- **package-lock.json**: Este arquivo é gerado automaticamente pelo npm e trava as versões exatas das dependências instaladas para garantir consistência nas instalações.

- **package.json**: Este é o arquivo de manifesto do projeto que lista as dependências, scripts, metadados e outras configurações do projeto.

- **postcss.config.js**: Este é o arquivo de configuração do PostCSS, uma ferramenta para transformação de estilos com JavaScript, usado para configurar plugins e opções do PostCSS.

- **README.md**: Este é o arquivo de documentação do projeto, que fornece informações sobre como configurar, instalar e usar o projeto.

- **tailwind.config.js**: Este é o arquivo de configuração do Tailwind CSS, um framework de CSS utility-first, usado para personalizar as configurações e comportamentos do Tailwind CSS.

- **tsconfig.json**: Este é o arquivo de configuração TypeScript para o projeto, utilizado para configurar o compilador TypeScript.

- **tsconfig.node.json**: Este é o arquivo de configuração TypeScript específico para ambiente de execução Node.js.

- **vite.config.js**: Este é o arquivo de configuração do Vite, utilizado para definir opções de compilação, plugins, entre outros, para o Vite.

