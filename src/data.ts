import coordOne from "./assets/fotos/coordenacao/image 31.png";
import coordTwo from "./assets/fotos/coordenacao/Artur Rozestraten-2 1.png";
import coordThree from "./assets/fotos/coordenacao/image 31_abbud.png";
import coordFour from "./assets/fotos/coordenacao/baixa-quadrado-luci-hidaka-2024 1.png";
import Photo1 from "./assets/fotos/equipe/Beatriz-Siqueira-Bueno-1.png";
import Photo2 from "./assets/fotos/equipe/Leandro-Veloso-1.png";
import DefaultPhoto from "./assets/fotos/equipe/image_33.png";
import kaiqueKomata from "./assets/fotos/equipe/Kaique-Komata-1.png";
import joaoDaniel from "./assets/fotos/equipe/Joao-Francisco-Lino-Daniel-1.png";
import deidsonRafael from "./assets/fotos/equipe/Deidson-Rafael-1.png";
import gustavoMachado from "./assets/fotos/equipe/gustavo-machado.png";
import brunoSouza from "./assets/fotos/equipe/Bruno-Souza-1.png";
import eikeSouza from "./assets/fotos/equipe/Eike-Souza-da-Silva-1.png";
import odairOliveira from "./assets/fotos/equipe/odair.png";
import abbudEquipe from "./assets/fotos/equipe/abbud-equipe.png";
import luciEquipe from "./assets/fotos/equipe/baixa-quadrado-luci-hidaka-2024-2.png";
import joaoLembo from "./assets/fotos/equipe/Joao-Gabriel-Loureiro-de-Lima-Lembo-1.png";
import vitorLima from "./assets/fotos/equipe/vitor-lima.png";
import harleyMacedo from "./assets/fotos/equipe/harley-macedo.png";

// LOGOS INSTITUTOS:
import CEBIMARLogo from "./assets/logos/logo_cebimar.png";
import CENALogo from "./assets/logos/logo_cena.png";
import ECALogo from "./assets/logos/logo_eca.png";
import EELogo from "./assets/logos/logo_ee.jpg";
import EEFELogo from "./assets/logos/logo_eefe.jpg";
import EEFERPLogo from "./assets/logos/logo_eeferp.jpeg";
import EELLogo from "./assets/logos/logo_eel.svg";
import EERPLogo from "./assets/logos/logo_eerp.png";
import EESCLogo from "./assets/logos/logo_eesc.png";
import ESALQLogo from "./assets/logos/logo_esalq.png";
import FAULogo from "./assets/logos/logo_fau.png";
import FCFLogo from "./assets/logos/logo_fcf.jpg";
import FCFRPLogo from "./assets/logos/logo_fcfrp.png";
import FDRPLogo from "./assets/logos/logo_fdrp.png";
import FEALogo from "./assets/logos/logo_fea.png";
import FEARPLogo from "./assets/logos/logo_fearp.png";
import FELogo from "./assets/logos/logo_fe.png";
import FFCLRPLogo from "./assets/logos/logo_ffclrp.png";
import FFLCHLogo from "./assets/logos/logo_fflch.png";
import FMBRULogo from "./assets/logos/logo_fmbru.jpg";
import FMLogo from "./assets/logos/logo_fm.jpg";
import FMRPLogo from "./assets/logos/logo_fmrp.png";
import FOBLogo from "./assets/logos/logo_fob.png";
import FOLogo from "./assets/logos/logo_fo.png";
import FORPLogo from "./assets/logos/logo_forp.png";
import FSPLogo from "./assets/logos/logo_fsp.png";
import FZEALogo from "./assets/logos/logo_fzea.png";
import FMVZLogo from "./assets/logos/logo_fmvz.jpg";
import IAGLogo from "./assets/logos/logo_iag.jpeg";
import IAULogo from "./assets/logos/logo_iau.png";
import IBCLogo from "./assets/logos/logo_ib.png";
import ICBLogo from "./assets/logos/logo_icb.png";
import ICMCLogo from "./assets/logos/logo_icmc.jpeg";
import IEAELogo from "./assets/logos/logo_iea.png";
import IEBLogo from "./assets/logos/logo_ieb.jpg";
import IEELLogo from "./assets/logos/logo_iee.png";
import IFLogo from "./assets/logos/logo_if.png";
import IFSCLogo from "./assets/logos/logo_ifsc.png";
import IGCLogo from "./assets/logos/logo_igc.png";
import IMELogo from "./assets/logos/logo_ime.png";
import IMTLogo from "./assets/logos/logo_imt.png";
import IOLogo from "./assets/logos/logo_io.png";
import IPLogo from "./assets/logos/logo_ip.png";
import IQLogo from "./assets/logos/logo_iq.png";
import IQSCLogo from "./assets/logos/logo_iqsc.jpg";
import IRILogo from "./assets/logos/logo_iri.png";
import POLILogo from "./assets/logos/logo_poli.jpg";



// IMAGENS PLANO DE FUNDO:
import CEBIMARPlanodeFundo from "./assets/fotos/institutos/fundocard_cebimar.png";
import CENAPlanodeFundo from "./assets/fotos/institutos/fundocard_cena.jpeg";
import ECAPlanodeFundo from "./assets/fotos/institutos/fundocard_eca.png";
import EEPlanodeFundo from "./assets/fotos/institutos/fundocard_EE.jpg";
import EEFEPlanodeFundo from "./assets/fotos/institutos/fundocard_eefe.jpg";
import EEFERPPlanodeFundo from "./assets/fotos/institutos/fundocard_eeferp.png";
import EELPlanodeFundo from "./assets/fotos/institutos/fundocard_eel.jpg";
import EERPPlanodeFundo from "./assets/fotos/institutos/fundocard_eerp.jpg";
import EESCPlanodeFundo from "./assets/fotos/institutos/fundocard_eesc.jpg";
import ESALQPlanodeFundo from "./assets/fotos/institutos/fundocard_esalq.jpg";
import FAUPlanodeFundo from "./assets/fotos/institutos/fundocard_fau.png";
import FCFPlanodeFundo from "./assets/fotos/institutos/fundocard_fcf.jpg";
import FCFRPPlanodeFundo from "./assets/fotos/institutos/fundocard_fcfrp.jpg";
import FDRPPlanodeFundo from "./assets/fotos/institutos/fundocard_fdrp.png";
import FEAPlanodeFundo from "./assets/fotos/institutos/fundocard_fea.jpg";
import FEARPPlanodeFundo from "./assets/fotos/institutos/fundocard_fearp.png";
import FEPlanodeFundo from "./assets/fotos/institutos/fundocard_fe.jpg";
import FFCLRPPlanodeFundo from "./assets/fotos/institutos/fundocard_ffclrp.jpg";
import FFLCHPlanodeFundo from "./assets/fotos/institutos/fundocard_fflch.jpg";
import FMBRUPlanodeFundo from "./assets/fotos/institutos/fundocard_FMBRU.jpg";
import FMPlanodeFundo from "./assets/fotos/institutos/fundocard_fm.png";
import FMRPPlanodeFundo from "./assets/fotos/institutos/fundocard_fmrp.png";
import FOBPlanodeFundo from "./assets/fotos/institutos/fundocard_fob.jpg";
import FOPlanodeFundo from "./assets/fotos/institutos/fundocard_fo.png";
import FORPPlanodeFundo from "./assets/fotos/institutos/fundocard_forp.jpg";
import FSPPlanodeFundo from "./assets/fotos/institutos/fundocard_fsp.jpg";
import FZEAPlanodeFundo from "./assets/fotos/institutos/fundocard_fzea.png";
import FMZVPlanodeFundo from "./assets/fotos/institutos/fundocard_fmzv.jpg";
import IAGPlanodeFundo from "./assets/fotos/institutos/fundocard_iag.jpg";
import IAUPlanodeFundo from "./assets/fotos/institutos/fundocard_iau.png";
import IBPlanodeFundo from "./assets/fotos/institutos/fundocard_ib.jpg";
import ICBPlanodeFundo from "./assets/fotos/institutos/fundocard_icb.jpg";
import ICMCPlanodeFundo from "./assets/fotos/institutos/fundocard_icmc.jpg";
import IEAPlanodeFundo from "./assets/fotos/institutos/fundocard_iea.jpg";
import IEBPlanodeFundo from "./assets/fotos/institutos/fundocard_ieb.jpg";
import IEEPlanodeFundo from "./assets/fotos/institutos/fundocard_iee.png";
import IFPlanodeFundo from "./assets/fotos/institutos/fundocard_if.png";
import IFSCPlanodeFundo from "./assets/fotos/institutos/fundocard_ifsc.jpg";
import IGCPlanodeFundo from "./assets/fotos/institutos/fundocard_igc.jpg";
import IMEPlanodeFundo from "./assets/fotos/institutos/fundocard_ime.png";
import IMTPlanodeFundo from "./assets/fotos/institutos/fundocard_imt.jpeg";
import IOPlanodeFundo from "./assets/fotos/institutos/fundocard_io.png";
import IPPlanodeFundo from "./assets/fotos/institutos/fundocard_ip.jpg";
import IQPlanodeFundo from "./assets/fotos/institutos/fundocard_iq.jpg";
import IQSCPlanodeFundo from "./assets/fotos/institutos/fundocard_iqsc.jpeg";
import IRIPlanodeFundo from "./assets/fotos/institutos/fundocard_iri.jpg";
import POLIPlanodeFundo from "./assets/fotos/institutos/fundocard_poli.jpg";
import { InstitutoCardsProps } from "./components/InstitutoCards/InstitutoCards";


const options_local_values = [
    "cidade_universitaria_butanta_sp",
    "each_usp_leste_sp",
    "largo_sao_francisco_sp",
    "quadrilatero_da_saude_sp",
    "ribeirao_preto",
    "sao_carlos",
    "pirassununga",
];

const options_area_values = [
    "ciencias_agrarias",
    "ciencias_biologicas",
    "ciencias_da_saude",
    "ciencias_exatas",
    "ciencias_humanas",
    "ciencias_naturais",
    "engenharias",
    "ciencias_sociais_aplicadas",
    "linguistica_letras_e_artes"
];


export const InstitutoCardsData: InstitutoCardsProps[] = [ 
    {
        logo: IMELogo,
        alt_logo: "Logo do Instituto de Matemática e Estatística",
        plano_de_fundo: IMEPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Instituto de Matemática e Estatística",
        nome_instituto: "Instituto de Matemática e Estatística",
        local: options_local_values[0],
        sigla: "IME",
        num_docentes: 200,
        num_pesquisas: 300,
        link: "http://cpqs-abertas-ime.s3-website-sa-east-1.amazonaws.com/",
        disponivel: true,
        area: options_area_values[3],
        palavras_chave: ["Programação", "Computação", "Matemática", "Desenvolvimento", "Estatística", "Exatas", "Lógica", "Algoritmos", "IME"],
    },
    {
        logo: FAULogo,
        alt_logo: "Logo da Faculdade de Arquitetura e Urbanismo",
        plano_de_fundo: FAUPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Faculdade de Arquitetura e Urbanismo",
        nome_instituto: "Faculdade de Arquitetura e Urbanismo",
        local: options_local_values[0],
        sigla: "FAU",
        num_docentes: 200,
        num_pesquisas: 300,
        link: "http://cpqs-abertas-fau.s3-website-sa-east-1.amazonaws.com/",
        disponivel: true,
        area: options_area_values[4],
        palavras_chave: ["Arquitetura", "Urbanismo", "Design", "Habitacao", "Design Gráfico", "Design de Serviço", "Sustentabilidade", "Design Digital", "FAU"],
    },
    {
        logo: IOLogo,
        alt_logo: "Logo do Instituto Oceanográfico",
        plano_de_fundo: IOPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Instituto Oceanográfico",
        nome_instituto: "Instituto Oceanográfico",
        local: options_local_values[0],
        sigla: "IO",
        num_docentes: 200,
        num_pesquisas: 300,
        link: "http://cpqs-abertas-io.s3-website-sa-east-1.amazonaws.com/",
        disponivel: true,
        area: options_area_values[5],
        palavras_chave: ["Oceanografia", "Biologia Marinha", "Ambiente Marinho", "Ecossistemas", "Oceanos", "Mares", "Biodiversidade", "Conservação", "IO"],
    },
    {
        logo: FDRPLogo,
        alt_logo: "Logo da Faculdade de Direito de Ribeirão Preto",
        plano_de_fundo: FDRPPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Faculdade de Direito de Ribeirão Preto",
        nome_instituto: "Faculdade de Direito de Ribeirão Preto",
        local: options_local_values[4],
        sigla: "FDRP",
        num_docentes: 200,
        num_pesquisas: 300,
        link: "http://cpqs-abertas-fdrp.s3-website-sa-east-1.amazonaws.com/",
        disponivel: true,
        area: options_area_values[4],
        palavras_chave: ["Direito", "Justiça", "Leis", "Advocacia", "Legislação", "Jurídico", "Constitucional", "Civil", "Advogado", "Advogada", "FDRP"],
    }
    ,
    {
        logo: IAULogo,
        alt_logo: "Logo do Instituto de Arquitetura e Urbanismo",
        plano_de_fundo: IAUPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Instituto de Arquitetura e Urbanismo",
        nome_instituto: "Instituto de Arquitetura e Urbanismo",
        local: options_local_values[5],
        sigla: "IAU",
        num_docentes: 100,
        num_pesquisas: 150,
        link: "http://cpqs-abertas-iau.s3-website-sa-east-1.amazonaws.com/",
        disponivel: true,
        area: options_area_values[4],
        palavras_chave: ["Arquitetura", "Urbanismo", "Design", "Habitacao", "Design Gráfico", "Design de Serviço", "Sustentabilidade", "Design Digital"],
    },
    {
        logo: IFLogo,
        alt_logo: "Logo do Instituto de Física",
        plano_de_fundo: IFPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Instituto de Física",
        nome_instituto: "Instituto de Física",
        local: options_local_values[0],
        sigla: "IF",
        num_docentes: 250,
        num_pesquisas: 400,
        link: "http://cpqs-abertas-fisica.s3-website-sa-east-1.amazonaws.com/",
        disponivel: true,
        area: options_area_values[3],
        palavras_chave: ["Física", "Química", "Matemática", "Astronomia", "Astrofísica", "Física Nuclear", "Física de Partículas", "Física Teórica"],
    },
    {
        logo: FZEALogo,
        alt_logo: "Logo da Faculdade de Zootecnia e Engenharia de Alimentos",
        plano_de_fundo: FZEAPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Faculdade de Zootecnia e Engenharia de Alimentos",
        nome_instituto: "Faculdade de Zootecnia e Engenharia de Alimentos",
        local: options_local_values[6],
        sigla: "FZEA",
        num_docentes: 150,
        num_pesquisas: 200,
        link: "http://cpqs-abertas-fzea.s3-website-sa-east-1.amazonaws.com/",
        disponivel: true,
        area: options_area_values[1],
        palavras_chave: ["Zootecnia", "Engenharia de Alimentos", "Agronomia", "Agricultura", "Pecuária", "Alimentos", "Nutrição", "Biotecnologia"],
    },
    {
        logo: FOLogo,
        alt_logo: "Logo da Faculdade de Odontologia",
        plano_de_fundo: FOPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Faculdade de Odontologia",
        nome_instituto: "Faculdade de Odontologia",
        local: options_local_values[0],
        sigla: "FO",
        num_docentes: 100,
        num_pesquisas: 150,
        link: "http://usp-aberta-fousp.s3-website-sa-east-1.amazonaws.com/",
        disponivel: true,
        area: options_area_values[2],
        palavras_chave: ["Odontologia", "Dentista", "Saúde Bucal", "Cirurgia", "Implantes", "Próteses", "Odonto"],
    },
    {
        logo: FEARPLogo,
        alt_logo: "Logo da Faculdade de Economia, Administração e Contabilidade de Ribeirão Preto",
        plano_de_fundo: FEARPPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Faculdade de Economia, Administração e Contabilidade de Ribeirão Preto",
        nome_instituto: "Faculdade de Economia, Administração e Contabilidade de Ribeirão Preto",
        local: options_local_values[4],
        sigla: "FEARP",
        num_docentes: 120,
        num_pesquisas: 180,
        link: "http://cpqs-abertas-fearp.s3-website-sa-east-1.amazonaws.com/",
        disponivel: true,
        area: options_area_values[4],
        palavras_chave: ["Economia", "Administração", "Contabilidade", "Finanças", "Marketing", "Gestão", "Negócios", "Empreendedorismo"],
    },
    {
        logo: CEBIMARLogo,
        alt_logo: "Logo do Centro de Biologia Marinha",
        plano_de_fundo: CEBIMARPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Centro de Biologia Marinha",
        nome_instituto: "Centro de Biologia Marinha",
        local: "São Sebastião",
        sigla: "CEBIMAR",
        num_docentes: 50,
        num_pesquisas: 80,
        link: "/cebimar",
    },
    {
        logo: CENALogo,
        alt_logo: "Logo do Centro de Energia Nuclear na Agricultura",
        plano_de_fundo: CENAPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Centro de Energia Nuclear na Agricultura",
        nome_instituto: "Centro de Energia Nuclear na Agricultura",
        local: "Piracicaba",
        sigla: "CENA",
        num_docentes: 80,
        num_pesquisas: 120,
        link: "/cena"
    },
    {
        logo: ECALogo,
        alt_logo: "Logo da Escola de Comunicações e Artes",
        plano_de_fundo: ECAPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Escola de Comunicações e Artes",
        nome_instituto: "Escola de Comunicações e Artes",
        local: "Butantã",
        sigla: "ECA",
        num_docentes: 150,
        num_pesquisas: 200,
        link: "/eca",
    },
    {
        logo: EELogo,
        alt_logo: "Logo da Escola de Enfermagem",
        plano_de_fundo: EEPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Escola de Enfermagem",
        nome_instituto: "Escola de Enfermagem",
        local: "Butantã",
        sigla: "EE",
        num_docentes: 100,
        num_pesquisas: 150,
        link: "/ee",
    },
    {
        logo: EEFELogo,
        alt_logo: "Logo da Escola de Educação Física e Esporte",
        plano_de_fundo: EEFEPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Escola de Educação Física e Esporte",
        nome_instituto: "Escola de Educação Física e Esporte",
        local: options_area_values[0],
        sigla: "EEFE",
        num_docentes: 100,
        num_pesquisas: 150,
        link: "/eefe",
        area: options_area_values[2],
        palavras_chave: ["Educação Física", "Esporte", "Atividade Física", "Saúde", "Bem-estar", "Treinamento", "Fisiologia", "Nutrição"],
    },
    {
        logo: EEFERPLogo,
        alt_logo: "Logo da Escola de Enfermagem de Ribeirão Preto",
        plano_de_fundo: EEFERPPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Escola de Enfermagem de Ribeirão Preto",
        nome_instituto: "Escola de Enfermagem de Ribeirão Preto",
        local: "Ribeirão Preto",
        sigla: "EEFERP",
        num_docentes: 80,
        num_pesquisas: 120,
        link: "/eefrp"
    },
    {
        logo: EESCLogo,
        alt_logo: "Logo da Escola de Engenharia de São Carlos",
        plano_de_fundo: EESCPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Escola de Engenharia de São Carlos",
        nome_instituto: "Escola de Engenharia de São Carlos",
        local: "São Carlos",
        sigla: "EESC",
        num_docentes: 150,
        num_pesquisas: 200,
        link: "/eesc"
    },
    {
        logo: EEFERPLogo,
        alt_logo: "Logo da Escola de Enfermagem de Ribeirão Preto",
        plano_de_fundo: EEFERPPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Escola de Enfermagem de Ribeirão Preto",
        nome_instituto: "Escola de Enfermagem de Ribeirão Preto",
        local: "Ribeirão Preto",
        sigla: "EEFERP",
        num_docentes: 80,
        num_pesquisas: 120,
        link: "/eefrp"
    },
    {
        logo: EELLogo,
        alt_logo: "Logo da Escola de Engenharia de Lorena",
        plano_de_fundo: EELPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Escola de Engenharia de Lorena",
        nome_instituto: "Escola de Engenharia de Lorena",
        local: "Lorena",
        sigla: "EEL",
        num_docentes: 130,
        num_pesquisas: 250,
        link: "/eel"
    },
    {
        logo: EERPLogo,
        alt_logo: "Logo da Escola de Enfermagem de Ribeirão Preto",
        plano_de_fundo: EERPPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Escola de Enfermagem de Ribeirão Preto",
        nome_instituto: "Escola de Enfermagem de Ribeirão Preto",
        local: "Ribeirão Preto",
        sigla: "EERP",
        num_docentes: 100,
        num_pesquisas: 140,
        link: "/eerp"
    },
    {
        logo: ESALQLogo,
        alt_logo: "Logo da Escola Superior de Agricultura Luiz de Queiroz",
        plano_de_fundo: ESALQPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Escola Superior de Agricultura Luiz de Queiroz",
        nome_instituto: "Escola Superior de Agricultura Luiz de Queiroz",
        local: "Piracicaba",
        sigla: "ESALQ",
        num_docentes: 200,
        num_pesquisas: 300,
        link: "/esalq"
    },
    {
        logo: FCFLogo,
        alt_logo: "Logo da Faculdade de Ciências Farmacêuticas",
        plano_de_fundo: FCFPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Faculdade de Ciências Farmacêuticas",
        nome_instituto: "Faculdade de Ciências Farmacêuticas",
        local: "Butantã",
        sigla: "FCF",
        num_docentes: 120,
        num_pesquisas: 180,
        link: "/fcf"
    },
    {
        logo: FCFRPLogo,
        alt_logo: "Logo da Faculdade de Ciências Farmacêuticas de Ribeirão Preto",
        plano_de_fundo: FCFRPPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Faculdade de Ciências Farmacêuticas de Ribeirão Preto",
        nome_instituto: "Faculdade de Ciências Farmacêuticas de Ribeirão Preto",
        local: "Ribeirão Preto",
        sigla: "FCFRP",
        num_docentes: 100,
        num_pesquisas: 150,
        link: "/fcfrp"
    },
    {
        logo: FEALogo,
        alt_logo: "Logo da Faculdade de Economia, Administração e Contabilidade",
        plano_de_fundo: FEAPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Faculdade de Economia, Administração e Contabilidade",
        nome_instituto: "Faculdade de Economia, Administração e Contabilidade",
        local: "Butantã",
        sigla: "FEA",
        num_docentes: 200,
        num_pesquisas: 300,
        link: "/fea"
    },
    {
        logo: FELogo,
        alt_logo: "Logo da Faculdade de Educação",
        plano_de_fundo: FEPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Faculdade de Educação",
        nome_instituto: "Faculdade de Educação",
        local: "Butantã",
        sigla: "FE",
        num_docentes: 100,
        num_pesquisas: 150,
        link: "/fe"
    },
    {
        logo: FFCLRPLogo,
        alt_logo: "Logo da Faculdade de Filosofia, Ciências e Letras de Ribeirão Preto",
        plano_de_fundo: FFCLRPPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Faculdade de Filosofia, Ciências e Letras de Ribeirão Preto",
        nome_instituto: "Faculdade de Filosofia, Ciências e Letras de Ribeirão Preto",
        local: "Ribeirão Preto",
        sigla: "FFCLRP",
        num_docentes: 100,
        num_pesquisas: 150,
        link: "/ffclrp"
    },
    {
        logo: FFLCHLogo,
        alt_logo: "Logo da Faculdade de Filosofia, Letras e Ciências Humanas",
        plano_de_fundo: FFLCHPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Faculdade de Filosofia, Letras e Ciências Humanas",
        nome_instituto: "Faculdade de Filosofia, Letras e Ciências Humanas",
        local: "Butantã",
        sigla: "FFLCH",
        num_docentes: 200,
        num_pesquisas: 300,
        link: "/fflch"
    },
    {
        logo: FMBRULogo,
        alt_logo: "Logo da Faculdade de Medicina de Botucatu",
        plano_de_fundo: FMBRUPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Faculdade de Medicina de Botucatu",
        nome_instituto: "Faculdade de Medicina de Botucatu",
        local: "Botucatu",
        sigla: "FMBRU",
        num_docentes: 100,
        num_pesquisas: 150,
        link: "/fmbru"
    },
    {
        logo: FOBLogo,
        alt_logo: "Logo da Faculdade de Odontologia de Bauru",
        plano_de_fundo: FOBPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Faculdade de Odontologia de Bauru",
        nome_instituto: "Faculdade de Odontologia de Bauru",
        local: "Bauru",
        sigla: "FOB",
        num_docentes: 120,
        num_pesquisas: 180,
        link: "/fob"
    },
    {
        logo: FORPLogo,
        alt_logo: "Logo da Faculdade de Odontologia de Ribeirão Preto",
        plano_de_fundo: FORPPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Faculdade de Odontologia de Ribeirão Preto",
        nome_instituto: "Faculdade de Odontologia de Ribeirão Preto",
        local: "Ribeirão Preto",
        sigla: "FORP",
        num_docentes: 100,
        num_pesquisas: 150,
        link: "/forp"
    },
    {
        logo: FMVZLogo,
        alt_logo: "Logo da Faculdade de Medicina Veterinária e Zootecnia",
        plano_de_fundo: FMZVPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Faculdade de Medicina Veterinária e Zootecnia",
        nome_instituto: "Faculdade de Medicina Veterinária e Zootecnia",
        local: "Butantã",
        sigla: "FMVZ",
        num_docentes: 150,
        num_pesquisas: 250,
        link: "/fmvz"
    },
    {
        logo: IAGLogo,
        alt_logo: "Logo do Instituto de Astronomia, Geofísica e Ciências Atmosféricas",
        plano_de_fundo: IAGPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Instituto de Astronomia, Geofísica e Ciências Atmosféricas",
        nome_instituto: "Instituto de Astronomia, Geofísica e Ciências Atmosféricas",
        local: "Butantã",
        sigla: "IAG",
        num_docentes: 100,
        num_pesquisas: 150,
        link: "/iag"
    },
    {
        logo: FMLogo,
        alt_logo: "Logo da Faculdade de Medicina",
        plano_de_fundo: FMPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Faculdade de Medicina",
        nome_instituto: "Faculdade de Medicina",
        local: "Butantã",
        sigla: "FM",
        num_docentes: 300,
        num_pesquisas: 500,
        link: "/fm"
    },
    {
        logo: FMRPLogo,
        alt_logo: "Logo da Faculdade de Medicina de Ribeirão Preto",
        plano_de_fundo: FMRPPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Faculdade de Medicina de Ribeirão Preto",
        nome_instituto: "Faculdade de Medicina de Ribeirão Preto",
        local: "Ribeirão Preto",
        sigla: "FMRP",
        num_docentes: 200,
        num_pesquisas: 400,
        link: "/fmrp"
    },
    {
        logo: FSPLogo,
        alt_logo: "Logo da Faculdade de Saúde Pública",
        plano_de_fundo: FSPPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Faculdade de Saúde Pública",
        nome_instituto: "Faculdade de Saúde Pública",
        local: "Butantã",
        sigla: "FSP",
        num_docentes: 150,
        num_pesquisas: 250,
        link: "/fsp"
    },
    {
        logo: IBCLogo,
        alt_logo: "Logo do Instituto de Biociências",
        plano_de_fundo: IBPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Instituto de Biociências",
        nome_instituto: "Instituto de Biociências",
        local: "Butantã",
        sigla: "IB",
        num_docentes: 180,
        num_pesquisas: 260,
        link: "/ib"
    },
    {
        logo: ICBLogo,
        alt_logo: "Logo do Instituto de Ciências Biomédicas",
        plano_de_fundo: ICBPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Instituto de Ciências Biomédicas",
        nome_instituto: "Instituto de Ciências Biomédicas",
        local: "Butantã",
        sigla: "ICB",
        num_docentes: 150,
        num_pesquisas: 220,
        link: "/icb"
    },
    {
        logo: ICMCLogo,
        alt_logo: "Logo do Instituto de Ciências Matemáticas e de Computação",
        plano_de_fundo: ICMCPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Instituto de Ciências Matemáticas e de Computação",
        nome_instituto: "Instituto de Ciências Matemáticas e de Computação",
        local: "São Carlos",
        sigla: "ICMC",
        num_docentes: 200,
        num_pesquisas: 300,
        link: "/icmc"
    },
    {
        logo: IEAELogo,
        alt_logo: "Logo do Instituto de Estudos Avançados",
        plano_de_fundo: IEAPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Instituto de Estudos Avançados",
        nome_instituto: "Instituto de Estudos Avançados",
        local: "Butantã",
        sigla: "IEA",
        num_docentes: 50,
        num_pesquisas: 100,
        link: "/iea"
    },
    {
        logo: IEBLogo,
        alt_logo: "Logo do Instituto de Estudos Brasileiros",
        plano_de_fundo: IEBPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Instituto de Estudos Brasileiros",
        nome_instituto: "Instituto de Estudos Brasileiros",
        local: "Butantã",
        sigla: "IEB",
        num_docentes: 40,
        num_pesquisas: 80,
        link: "/ieb"
    },
    {
        logo: IEELLogo,
        alt_logo: "Logo do Instituto de Energia e Eletrotécnica",
        plano_de_fundo: IEEPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Instituto de Energia e Eletrotécnica",
        nome_instituto: "Instituto de Energia e Eletrotécnica",
        local: "Butantã",
        sigla: "IEE",
        num_docentes: 60,
        num_pesquisas: 120,
        link: "/iee"
    },
    {
        logo: IFSCLogo,
        alt_logo: "Logo do Instituto de Física de São Carlos",
        plano_de_fundo: IFSCPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Instituto de Física de São Carlos",
        nome_instituto: "Instituto de Física de São Carlos",
        local: "São Carlos",
        sigla: "IFSC",
        num_docentes: 150,
        num_pesquisas: 220,
        link: "/ifsc"
    },
    {
        logo: IGCLogo,
        alt_logo: "Logo do Instituto de Geociências",
        plano_de_fundo: IGCPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Instituto de Geociências",
        nome_instituto: "Instituto de Geociências",
        local: "Butantã",
        sigla: "IGC",
        num_docentes: 100,
        num_pesquisas: 150,
        link: "/igc"
    },
    {
        logo: IMTLogo,
        alt_logo: "Logo do Instituto de Medicina Tropical",
        plano_de_fundo: IMTPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Instituto de Medicina Tropical",
        nome_instituto: "Instituto de Medicina Tropical",
        local: "Butantã",
        sigla: "IMT",
        num_docentes: 80,
        num_pesquisas: 120,
        link: "/imt"
    },
    {
        logo: IPLogo,
        alt_logo: "Logo do Instituto de Psicologia",
        plano_de_fundo: IPPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Instituto de Psicologia",
        nome_instituto: "Instituto de Psicologia",
        local: "Butantã",
        sigla: "IP",
        num_docentes: 100,
        num_pesquisas: 150,
        link: "/ip"
    },
    {
        logo: IQSCLogo,
        alt_logo: "Logo do Instituto de Química de São Carlos",
        plano_de_fundo: IQSCPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Instituto de Química de São Carlos",
        nome_instituto: "Instituto de Química de São Carlos",
        local: "São Carlos",
        sigla: "IQSC",
        num_docentes: 120,
        num_pesquisas: 180,
        link: "/iqsc"
    },
    {
        logo: IRILogo,
        alt_logo: "Logo do Instituto de Relações Internacionais",
        plano_de_fundo: IRIPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Instituto de Relações Internacionais",
        nome_instituto: "Instituto de Relações Internacionais",
        local: "Butantã",
        sigla: "IRI",
        num_docentes: 60,
        num_pesquisas: 100,
        link: "/iri"
    },
    {
        logo: POLILogo,
        alt_logo: "Logo da Escola Politécnica",
        plano_de_fundo: POLIPlanodeFundo,
        alt_plano_de_fundo: "Fotografia da Escola Politécnica",
        nome_instituto: "Escola Politécnica",
        local: "Butantã",
        sigla: "POLI",
        num_docentes: 300,
        num_pesquisas: 500,
        link: "/poli"
    },
    {
        logo: IQLogo,
        alt_logo: "Logo do Instituto de Química",
        plano_de_fundo: IQPlanodeFundo,
        alt_plano_de_fundo: "Fotografia do Instituto de Química",
        nome_instituto: "Instituto de Química",
        local: "Butantã",
        sigla: "IQ",
        num_docentes: 180,
        num_pesquisas: 260,
        link: "/iq"
    },    

];



export const COORDENACAO = [
    {
        photo: coordOne,
        title: "Alfredo Goldman",
        description: "(IME/USP)",

    },

    {
        photo:coordTwo,
        title: "Artur Rozestraten",
        description: "(FAUD/USP)",

    },
];

export const COORDENACAO_DESIGN = [

        {
                photo: coordThree,
                title: "Luiz Felipe Abud",
                description: "(FAU/USP)",
        
        },
        
        {
                photo: coordFour,
                title: "Luci Hidaka",
                description: "(FAU/USP)",
        
        },
]

export const EQUIPE_2019 = [

    {
        photo: DefaultPhoto,
        title: "Amarílis Corrêa",
        description: "Colaboração",
    },

    {
        photo: Photo1,
        title: "Beatriz Bueno",
        description: "Colaboração",
    },
    
    {
        photo: DefaultPhoto,
        title: "César Fernandes",
        description: "Desenvolvimento",
    },
    
    {
        photo: harleyMacedo,
        title: "Harley Macedo",
        description: "Colaboração",
    },
    
    {
        photo: Photo2,
        title: "Leandro Velloso",
        description: "Colaboração",
    },

    {
        photo: DefaultPhoto,
        title: "Leonardo Aguilar",
        description: "Desenvolvimento",
    },

    {
        photo: DefaultPhoto,
        title: "Larissa Sala",
        description: "Desenvolvimento",
    },

    {
        photo: DefaultPhoto,
        title: "Mateus dos Anjos",
        description: "Desenvolvimento",
    },

    {
        photo: DefaultPhoto,
        title: "Matheus Cunha",
        description: "Desenvolvimento",
    },

    {
        photo: DefaultPhoto,
        title: "Nathalia Borin",
        description: "Desenvolvimento",
    },

    {
        photo: DefaultPhoto,
        title: "Pedro Santos",
        description: "Desenvolvimento",
    },

    {
        photo: DefaultPhoto,
        title: "Victor Batistella",
        description: "Desenvolvimento",
    },

];

export const EQUIPE_2020 = [
    {
        photo: deidsonRafael,
        title: "Deidson R. Trindade",
        description: "Colaboração",
    },

    {
        photo: kaiqueKomata,
        title: "Kaique Komata",
        description: "Desenvolvimento",
    },

    {
        photo: DefaultPhoto,
        title: "Jean Pereira",
        description: "Desenvolvimento",
    },

    {
        photo: Photo2,
        title: "Leandro Velloso",
        description: "Colaboração",
    },

    {
        photo: DefaultPhoto,
        title: "Luciana Marques",
        description: "Desenvolvimento"
    },

    {
        photo: abbudEquipe,
        title: "Luís Felipe Abbud",
        description: "Design",
    },

    {
        photo: DefaultPhoto,
        title: "Priscila Lima",
        description: "Desenvolvimento"
    }
];

export const EQUIPE_2021 = [

    {
        photo: joaoDaniel,
        title: "João Daniel",
        description: "Desenvolvimento",
    },

    {
        photo: joaoLembo,
        title: "João Gabriel",
        description: "Desenvolvimento",
    },

    {
        photo: DefaultPhoto,
        title: "Leonardo Pereira",
        description: "Desenvolvimento",
    },

    {
        photo: vitorLima,
        title: "Vitor Lima",
        description: "Desenvolvimento",
    },

    {
        photo: deidsonRafael,
        title: "Deidson Rafael",
        description: "Colaboração"
    },

    {
        photo: Photo2,
        title: "Leandro Velloso",
        description: "Colaboração",
    },

    {
        photo: gustavoMachado,
        title: "Gustavo Machado",
        description: "Design",
    },

    {
        photo: abbudEquipe,
        title: "Luís Felipe Abbud",
        description: "Coordenação",
    },


];

export const EQUIPE_2022 = [

    {
        photo: joaoDaniel,
        title: "João Daniel",
        description: "Desenvolvimento",
    },

    {
        photo: gustavoMachado,
        title: "Gustavo Machado",
        description: "Design",
    },

    {
        photo: Photo2,
        title: "Leandro Velloso",
        description: "Colaboração",
    },

    {
        photo: abbudEquipe,
        title: "Luís Felipe Abbud",
        description: "Coordenação",
    },

    {
        photo: DefaultPhoto,
        title: "Mohamad Rkein",
        description: "Desenvolvimento",
    },

    {
        photo: DefaultPhoto,
        title: "Rafael Rodrigues",
        description: "Desenvolvimento",
    },
];

export const EQUIPE_2023 = [

    {
        photo: brunoSouza,
        title: "Bruno Souza",
        description: "Design",
    },

    {
        photo: eikeSouza,
        title: "Eike Souza da Silva",
        description: "Desenvolvimento",
    },

    {
        photo: DefaultPhoto,
        title: "Fernanda Tieme Itoda",
        description: "Desenvolvimento",
    },

    {
        photo: DefaultPhoto,
        title: "Guilherme G. Lourenço",
        description: "Design",
    },

    {
        photo: odairOliveira,
        title: "Odair G. Oliveira",
        description: "Desenvolvimento",
    },

    {
        photo: DefaultPhoto,
        title: "Lucas de Sousa Tenorio",
        description: "Desenvolvimento",
    },

    {
        photo: abbudEquipe,
        title: "Luís Felipe Abbud",
        description: "Coordenação e Design",
    },

    {
        photo: DefaultPhoto,
        title: "Matheus Ribeiro Silva",
        description: "Desenvolvimento",
    },

    {
        photo: DefaultPhoto,
        title: "Paulo Meirelles",
        description: "Coordenação",
    },
];

export const EQUIPE_2024 = [

    {
        photo: eikeSouza,
        title: "Eike Souza da Silva",
        description: "Desenvolvimento",
    },

    {
        photo: luciEquipe,
        title: "Lucilene M. Hidaka",
        description: "Coordenação e Design",
    },

    {
        photo: odairOliveira,
        title: "Odair G. Oliveira",
        description: "Desenvolvimento",
    },
];

export const DUVIDAS = [
        {
                number: "01",
                question: " O que é o projeto CPqs Abertas ?",
                answer:  `O intuito principal deste projeto é dar visibilidade quantitativa e qualitativamente à produção intelectual da
                faculdade, difundindo sua especificidade e diversidade através de dados extraídos do currículo Lattes de 
                seus docentes, tanto para a comunidade interna, a USP, quanto para a comunidade externa, 
                contribuindo assim para uma melhor compreensão da sociedade em geral quanto ao trabalho e às contribuições 
                advindas das atividades desenvolvidas na Universidade.`
        },

        {
                number: "02",
                question: "Como são realizadas as coletas de informações do site ?",
                answer: `Os dados exibidos são provenientes do Lattes. nos casos de mapas, usamos como referência 
                a capital dos estados (ou países, se for o caso).`
        },

        {
                number: "03",
                question: "A quais institutos o projeto se vincula hoje ?",
                answer: `O projeto começou pela FAU e a segunda unidade a ser atendida foi a FEA-RP, seguida do IME-USP. Hoje o projeto atende
                as unidades IF-USP, FDRP-USP, IAU-USP e IO-USP .`
        },

        {
                number: "04",
                question: "Percebi incongruências nos meus dados apresentados na plataforma, o que devo fazer?",
                answer: `Reveja o preenchimento de seu CV Lattes. A completude e a precisão neste preenchimento são fundamentais para 
                a congruência dos dados apresentados. A próxima atualização do sistema deve apresentar as devidas correções.`
        },

        {
                number: "05",
                question: "Minha dúvida não está aqui. O que fazer ?",
                answer: `Caso você tenha alguma dúvida que não tenha sido respondida, não hesite em nos contatar! 
                Basta preencher as informações contidas na aba Contato que te mandaremos um e-mail esclarecendo quaisquer perguntas.`
        }
];