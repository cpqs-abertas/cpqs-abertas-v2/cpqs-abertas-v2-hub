import "./styles/App.css"
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/Home.tsx';
import Header from "./components/Header/Header.tsx";
import About from "./pages/About/About.tsx";
import Footer from "./components/Footer/Footer.tsx";
import Unidades from "./pages/Unidades/Unidades.tsx";
import Resultados from "./pages/Resultados/Resultados.tsx";
import Contato from "./pages/Contato/Contato.tsx";

function App() {
  return (
    <>
      <Router>
        <Header />
        <Routes>
          <Route path="/" Component={Home} />
          <Route path="/sobre" Component={About} /> 
          <Route path="/unidades" Component={Unidades} />
          <Route path="/contato" Component={Contato} />
          <Route path="/resultados" element={<Resultados results={[]} />} />
        </Routes>
        <Footer />
      </Router>
    </>
  );
}

export default App;

