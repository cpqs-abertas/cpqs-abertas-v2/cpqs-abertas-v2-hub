import "./ListInstitutoCards.css"
import InstitutoCards from "@/components/InstitutoCards/InstitutoCards"
import { InstitutoCardsData } from "@/data"

function ListInstitutoCards({ filter }: { filter?: { local?: string, area?: string, palavras_chave?: string[] } }) {
    const filteredData = InstitutoCardsData.filter(instituto => 
        (!filter?.local || instituto.local.toLowerCase().includes(filter.local.toLowerCase())) &&
        (!filter?.area || (instituto.area ?? "").toLowerCase().includes(filter.area.toLowerCase())) &&
        (!filter?.palavras_chave || filter.palavras_chave.every(palavra => 
            (instituto.palavras_chave?.some(institutoPalavra => institutoPalavra.toLowerCase().includes(palavra.toLowerCase())) ?? false) ||
            instituto.nome_instituto.toLowerCase().includes(palavra.toLowerCase()) ||
            (instituto.area ?? "").toLowerCase().includes(palavra.toLowerCase()) ||
            instituto.local.toLowerCase().includes(palavra.toLowerCase())
        ))
    );

    return (
        <div className="listacards">
            {filteredData.length === 0 ? (
            <p>Nenhum instituto encontrado. Por favor, tente novamente com outros critérios de busca.</p>
            ) : (
                filteredData.map((instituto, index) => (
                    <div key={index}>
                        <InstitutoCards
                            logo={instituto.logo}
                            alt_logo={instituto.alt_logo}
                            plano_de_fundo={instituto.plano_de_fundo}
                            alt_plano_de_fundo={instituto.alt_plano_de_fundo}
                            nome_instituto={instituto.nome_instituto}
                            local={instituto.local}
                            sigla={instituto.sigla}
                            num_docentes={instituto.num_docentes}
                            num_pesquisas={instituto.num_pesquisas}
                            link={instituto.link}
                            disponivel={instituto.disponivel}
                            area={instituto.area}
                            palavras_chave={instituto.palavras_chave}
                        />
                    </div>
                ))
            )}
        </div>
    );
}

export default ListInstitutoCards