import "./HeaderDropdownMenu.css";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { 
    DropdownMenu, 
    DropdownMenuContent, 
    DropdownMenuItem, 
    DropdownMenuTrigger
} from "../ui/dropdown-menu";
import SearchBar from "./Searchbar/Searchbar";

interface HeaderDropdownMenuProps {
    logo: string;
    menuButton: string;
    closeButton: string;
    searchButton: string;
}


function HeaderDropdownMenu ({logo, menuButton, closeButton, searchButton} : HeaderDropdownMenuProps) {

    const navigate = useNavigate();
    const [menuClick, setMenuClick] = useState(false);
    const [searchClick, setSearchClick] = useState(false);

    function handleSearchClick(state : boolean) {
        setSearchClick(state);
}

    function handleMenuClick() {
        setMenuClick((menuClick) => !menuClick);
    }

    function handleButtonClick(path : string) {
        navigate(path);

    }

    return (
        <menu>
            {!searchClick ? (
                <>
                    <ul className= "c-header__menu">

                        <li>
                            <img src={logo} />
                        </li>

                        <div id="c-header_dropdown-search">

                            <li>
                                        <button onClick={() => handleSearchClick(true)}>
                                            <img src={searchButton} alt="busca"/>
                                        </button>
                            </li>

                            <li>
                                <DropdownMenu open={menuClick} onOpenChange={handleMenuClick}>

                                    <DropdownMenuTrigger>
                                        <img src={!menuClick ? `${menuButton}` : `${closeButton}`} alt="menu-hamburguer"/>
                                    </DropdownMenuTrigger>

                                    <DropdownMenuContent>
                                        <DropdownMenuItem onClick={() => handleButtonClick("/")}>Home</DropdownMenuItem>
                                        <DropdownMenuItem onClick={() => handleButtonClick("/sobre")}>Sobre</DropdownMenuItem>
                                        <DropdownMenuItem onClick={() => handleButtonClick("/unidades")}>Unidades</DropdownMenuItem>
                                        <DropdownMenuItem onClick={() => handleButtonClick("/contato")}>Contato</DropdownMenuItem> 
                                    </DropdownMenuContent>

                                </DropdownMenu>
                            </li>

                        </div>

                    </ul>
                </>
            ) : (
                    <div>
                        <SearchBar boxOnBlur={handleSearchClick}  onSearch={(searchTerm, results) => {console.log(searchTerm, results)}}/>
                        <img 
                            src={searchButton}
                            id={"c-searchbox__img"} 
                        />
                    </div>
                )
        }   
        </menu>
    );
}

export default HeaderDropdownMenu;