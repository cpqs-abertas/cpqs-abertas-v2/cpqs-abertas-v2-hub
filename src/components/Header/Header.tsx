import { useState } from "react";
import { useNavigate } from "react-router-dom";
import HeaderDropdownMenu from "./HeaderDropdownMenu";
import SearchBar from "./Searchbar/Searchbar";
import CNPqLogo from "../../assets/logos/cnpq-logo-black.png";
import closeButtonBlack from "../../assets/botoes/close-button.png";
import USPAbertaLogoBlack from "../../assets/logos/Logo-USP-aberta-black.png";
import HamburgerMenuBlack from "../../assets/botoes/hamburger-menu-black.png";
import MagnifyingGlassBlack from "../../assets/botoes/magnifying-glass-black.png";
import MediaQuery from "react-responsive";
// import HeaderButton from "./HeaderButton"; usado para controlar o estilo do botão ao ser selecionado, não apagar por enquanto
import "./Header.css";


function Header() {

        const [searchIsFocused, setSearchIsFocused] = useState(false);
        const navigate = useNavigate(); 

        function handleButtonClick(path : string) {
                navigate(path);
        } 

        function handleSearchBarRender(state : boolean) {
                setSearchIsFocused(state);
        }       

        return (
        <div>
                <div className="c-header">
                        <MediaQuery minWidth={800}>
                                <img className={"c-header__logo"} src={USPAbertaLogoBlack} alt="usp-logo" />
                                <menu className={"c-header__button"}>
                                {searchIsFocused ? <SearchBar boxOnBlur={handleSearchBarRender}  onSearch={(searchTerm, results) => {console.log(searchTerm, results)}}/> : (
                                        <>
                                                <button onClick={() => handleButtonClick("/")}>Home</button>
                                                <button onClick={() => handleButtonClick("/sobre")}>Sobre</button>
                                                <button onClick={() => handleButtonClick("/unidades")}>Unidades</button>
                                                <button onClick={() => handleButtonClick("/contato")}>Contato</button>
                                                <div className={"c-header__search-container"}>
                                                        <input
                                                                className={"c-header__search"}
                                                                onClick={() => handleSearchBarRender(true)}
                                                                type="search"
                                                                />
                                                        <img id={"search-icon"} src={MagnifyingGlassBlack} />
                                                </div>
                                        </>
                                )}
                                </menu>
                                <img className={"c-header__logo--height"} src={CNPqLogo} alt={"cnpq-logo"} />
                        </MediaQuery>
                        <MediaQuery minWidth={650} maxWidth={799}>
                                <HeaderDropdownMenu
                                        logo={USPAbertaLogoBlack}
                                        menuButton={HamburgerMenuBlack}
                                        closeButton={closeButtonBlack}
                                        searchButton={MagnifyingGlassBlack}
                                />
                        </MediaQuery>
                        <MediaQuery maxWidth={640}>
                                <HeaderDropdownMenu
                                        logo={USPAbertaLogoBlack}
                                        menuButton={HamburgerMenuBlack}
                                        closeButton={closeButtonBlack}
                                        searchButton={MagnifyingGlassBlack}
                                />
                        </MediaQuery>
                </div>
        </div>
                );
}

export default Header;