import { useEffect, useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import { InstitutoCardsData } from "@/data";
import { InstitutoCardsProps } from "@/components/InstitutoCards/InstitutoCards";
import "./Searchbar.css";
import MagnifyingGlass from "../../../assets/botoes/magnifying-glass-black.png";

interface SearchBarProps {
    boxOnBlur: (state : boolean) => void;
    onSearch: (searchTerm: string, results: InstitutoCardsProps[]) => void;
}

function SearchBar({ boxOnBlur, onSearch } : SearchBarProps) {

    const [searchTerm, setSearchTerm] = useState('');
    const [results, setResults] = useState<InstitutoCardsProps[]>([]);
    const [isFocused, setIsFocused] = useState(false);
    const navigate = useNavigate();

    const inputRef = useRef<HTMLInputElement>(null);
    
    useEffect(() => {
        if(inputRef.current){
            inputRef.current.focus();
        }

    }, []);

    useEffect(() => {
            if (searchTerm) {
                const filteredResults = InstitutoCardsData.filter(institute =>
                    institute.nome_instituto.toLowerCase().includes(searchTerm.toLowerCase()) ||
                    institute['palavras_chave']?.some(keyword =>
                        keyword.toLowerCase().includes(searchTerm.toLowerCase())
                    )
                );
                setResults(filteredResults);
            } else {
                setResults([]);
            }
        }, [searchTerm]);

        useEffect(() => {
                onSearch(searchTerm, results);
            }, [searchTerm, results, onSearch]);
        
            const handleResultClick = (nome: string) => {
                setSearchTerm(nome);
                setIsFocused(false);
                onSearch(nome, results);
            };
        
            const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
                if (e.key === 'Enter') {
                    navigate(`/resultados?search=${searchTerm}`);
                }
            };

        const handleClickEvent = () => {
                if(searchTerm.trim() !== ' ') {
                        navigate(`/resultados?search=${searchTerm}`);
                }
        }
    function handleSearchBarBlur() {
        if(!searchTerm){
                boxOnBlur(false);
        }
    }

    return (
        <div className={"searchbox-container"}>

            <input 
                className="searchbar"
                ref={inputRef}
                onBlur={handleSearchBarBlur}
                onChange={(e) => setSearchTerm(e.target.value)}
                onFocus={() => setIsFocused(true)}
                onKeyDown={handleKeyDown}
                type="search" 
                value={searchTerm}
                placeholder="Nome do instituto..." 
            />
            <a onClick={handleClickEvent}>
                <img  alt={"botão de busca"}id={"search-icon-expanded"} src={MagnifyingGlass} />
            </a>
            {isFocused && results.length > 0 && (
                    <div className="results-box">
                                {results.map((result, index) => (
                                <div 
                                    key={index} 
                                    className="result-item"
                                    onMouseDown={() => handleResultClick(result.nome_instituto)}
                                    >
                                            {result.nome_instituto}
                                </div>
                                ))}
                    </div>
                )}
        </div>
    );
}

export default SearchBar;