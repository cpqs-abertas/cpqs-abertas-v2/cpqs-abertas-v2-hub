import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import "./Searchbar2.css";
import { InstitutoCardsData } from "@/data"
import { InstitutoCardsProps } from '@/components/InstitutoCards/InstitutoCards';
import MagniFyingGlass from "../../../assets/botoes/magnifying-glass-black.png";

interface SearchBar2Props {
    onSearch: (searchTerm: string, results: InstitutoCardsProps[]) => void;
}

const SearchBar2: React.FC<SearchBar2Props> = ({ onSearch }) => {
    const [searchTerm, setSearchTerm] = useState('');
    const [results, setResults] = useState<InstitutoCardsProps[]>([]);
    const [isFocused, setIsFocused] = useState(false);
    const navigate = useNavigate();

    useEffect(() => {
        if (searchTerm) {
            const filteredResults = InstitutoCardsData.filter(institute =>
                institute.nome_instituto.toLowerCase().includes(searchTerm.toLowerCase()) ||
                institute['palavras_chave']?.some(keyword =>
                    keyword.toLowerCase().includes(searchTerm.toLowerCase())
                )
            );
            setResults(filteredResults);
        } else {
            setResults([]);
        }
    }, [searchTerm]);

    useEffect(() => {
        onSearch(searchTerm, results);
    }, [searchTerm, results, onSearch]);

    const handleResultClick = (nome: string) => {
        setSearchTerm(nome);
        setIsFocused(false);
        onSearch(nome, results);
    };

    const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
            navigate(`/resultados?search=${searchTerm}`);
        }
    };

    return (
        <div className={`searchbar-container ${searchTerm ? 'searchbar-large' : ''}`}>
            <div className='searchbar'>
                <img src={MagniFyingGlass} alt="Buscar"/>
                <input
                    type="text"
                    value={searchTerm}
                    onChange={(e) => setSearchTerm(e.target.value)}
                    onFocus={() => setIsFocused(true)}
                    onKeyDown={handleKeyDown}
                    placeholder="Buscar..."
                />
            </div>
            {isFocused && results.length > 0 && (
                <div className="results-box">
                    {results.map((result, index) => (
                        <div 
                            key={index} 
                            className="result-item"
                            onMouseDown={() => handleResultClick(result.nome_instituto)}
                        >
                            {result.nome_instituto}
                        </div>
                    ))}
                </div>
            )}
        </div>
    );
};

export default SearchBar2;
