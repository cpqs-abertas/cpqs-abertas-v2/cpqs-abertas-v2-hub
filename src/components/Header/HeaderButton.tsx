import {  ReactNode } from "react";

interface HeaderButtonProps {
        children: ReactNode;
        onSelect: React.MouseEventHandler;
        isClicked: boolean;

}

function HeaderButton({ children, onSelect, isClicked  } : HeaderButtonProps) {
        return (
                <>
                        <button onClick={onSelect} className={`${isClicked ? "selected": ""}`}>{children}</button>
                </>
        );

}

export default HeaderButton;