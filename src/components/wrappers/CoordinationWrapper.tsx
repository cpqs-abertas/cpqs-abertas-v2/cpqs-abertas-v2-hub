interface CoordinationWrapperProps {
    photo: string;
    title: string;
    description: string;
}

function CoordinationWrapper({ photo, title, description } : CoordinationWrapperProps) {

    return (
        <li>
            <img src={photo} alt={title} />
            <h3>{title}</h3>
            <p>{description}</p>
        </li>
    );

}

export default CoordinationWrapper;