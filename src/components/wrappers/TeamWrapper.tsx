interface TeamWrapperProps {
    photo: string,
    title: string,
    description: string,
}
function TeamWrapper({ photo, title, description } : TeamWrapperProps) {
    return (
        <li>
            <img src={photo} alt="team-member-picture" />
            <h3>{title}</h3>
            <p>{description}</p>
        </li>
    );
}

export default TeamWrapper