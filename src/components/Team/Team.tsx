import "./Team.css";
import TeamWrapper from "@/components/wrappers/TeamWrapper";
import { 
    EQUIPE_2019,
    EQUIPE_2020,
    EQUIPE_2021,
    EQUIPE_2022,
    EQUIPE_2023,
    EQUIPE_2024,

} from "@/data";
import { useState } from "react";
import ExpandButton from "@/assets/botoes/botao.svg";

interface TeamProps {
    year: string
}

function Team({ year } : TeamProps) {
    const [teamActive, setTeamActive] = useState(false);

    const handleTeamButton = () => {
        setTeamActive(!teamActive);
    }

    function renderTeam() {
        switch(year) {
            case "2019":
                return (
                    EQUIPE_2019.map((person) => (
                        <TeamWrapper key={person.title} {...person} />
                    ))
                );
            
            case "2020":
                return (
                    EQUIPE_2020.map((person) => (
                        <TeamWrapper key={person.title} {...person} />
                    ))
                );
            
            case "2021":
                return (
                    EQUIPE_2021.map((person) => (
                        <TeamWrapper key={person.title} {...person} />
                    ))
                );
            
            case "2022":
                return (
                    EQUIPE_2022.map((person) => (
                        <TeamWrapper key={person.title} {...person} />
                    ))
                );
            
            case "2023":
                return (
                    EQUIPE_2023.map((person) => (
                        <TeamWrapper key={person.title} {...person} />
                    ))
                );
            
            case "2024":
                return (
                    EQUIPE_2024.map((person) => (
                        <TeamWrapper key={person.title} {...person} />
                    ))
                );
        }
    }

    return(
        <section className={`c-team ${!teamActive ? "" : "expand"}`}>
            <div className="c-team__head">
                <h2>{year}</h2>
                <button 
                    onClick={() => handleTeamButton()}
                    className={`c-team__headButton ${teamActive ? "up" : ""}`}>
                            <img src={ExpandButton} />
                </button>
            </div>
            {teamActive &&
                <ol className="c-team__equipe">
                    {renderTeam()}
                </ol>
            }
        </section>

    );
}

export default Team;