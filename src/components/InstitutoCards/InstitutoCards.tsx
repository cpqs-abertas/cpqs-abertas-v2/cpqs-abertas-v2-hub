import "./InstitutoCards.css";

export interface InstitutoCardsProps {
    logo: string;
    alt_logo: string;
    plano_de_fundo: string;
    alt_plano_de_fundo: string;
    nome_instituto: string;
    local: string;
    sigla: string;
    num_docentes: number;
    num_pesquisas: number;
    link: string;
    disponivel?: boolean;
    area?: string | null;
    palavras_chave?: string[] | null;
}


function InstitutoCards({ logo, alt_logo, plano_de_fundo, alt_plano_de_fundo, nome_instituto, local, sigla, num_docentes, num_pesquisas, link, disponivel=false }: InstitutoCardsProps) {
    return (
        <div className="institutocard">
            <a href={link}>
                <div className="institutocard__planodefundo-wrapper">
                    <img className={`institutocard__planodefundo ${!disponivel ? 'institutocard__planodefundo--indisponivel' : ''}`} src={plano_de_fundo} alt={alt_plano_de_fundo} />
                    {!disponivel && <div className="institutocard__overlay">EM BREVE</div>}
                </div>
                <div className="institutocard__info">
                    <img className="institutocard__logo" src={logo} alt={alt_logo} />
                    <h2 className="institutocard__titulo">{nome_instituto}</h2>
                </div>
        
                <div className="institutocard__extrainfo--hover">
                    <p className="institutocard__sigla">{sigla} | {local}</p>
                    <p className="institutocard__numdocentes">{num_docentes} Docentes</p>
                    <p className="institutocard__numpesquisas">{num_pesquisas} Pesquisas</p>
                    <a href={link}>Saiba mais</a>
                </div>
            </a>
        </div>
    );
}

export default InstitutoCards;