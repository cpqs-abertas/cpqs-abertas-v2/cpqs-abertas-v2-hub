import MediaQuery from "react-responsive";
import CNPQLogo from "../../assets/logos/logo_cnpqbranca.png";
import "./Footer.css";

function Footer() {
    return (
        <div className="c-footer">
            <MediaQuery maxWidth={744}>
                <img src={CNPQLogo} alt="cnpq-logo" />
            </MediaQuery>

        </div>

    );
}

export default Footer;