import ListInstitutoCards from "@/components/ListInstitutoCards/ListInstitutoCards";
import "./Filtros.css";
import { Combobox } from "@/components/Filter/filter";
import React from "react";

export const options_local = [
    { value: "cidade_universitaria_butanta_sp", label: "Cidade Universitária / Butantã (SP)" },
    { value: "each_usp_leste_sp", label: "EACH / USP Leste (SP)" },
    { value: "largo_sao_francisco_sp", label: "Largo São Francisco (SP)" },
    { value: "quadrilatero_da_saude_sp", label: "Quadrilátero da Saúde (SP)" },
    { value: "ribeirao_preto", label: "Ribeirão Preto" },
    { value: "sao_carlos", label: "São Carlos" },
    { value: "pirassununga", label: "Pirassununga" }
]

export const options_area = [
    { value: "ciencias_agrarias", label: "Ciências Agrárias" },
    { value: "ciencias_biologicas", label: "Ciências Biológicas" },
    { value: "ciencias_da_saude", label: "Ciências da Saúde" },
    { value: "ciencias_exatas", label: "Ciências Exatas" },
    { value: "engenharias", label: "Engenharias" },
    { value: "ciencias_humanas", label: "Ciências Humanas" },
    { value: "ciencias_naturais", label: "Ciências Naturais" },
    { value: "ciencias_sociais_aplicadas", label: "Ciências Sociais Aplicadas" },
    { value: "linguistica_letras_e_artes", label: "Lingüística, Letras e Artes" },
]

interface FilterComponentProps {
    selectedLocal: string;
    setSelectedLocal: React.Dispatch<React.SetStateAction<string>>;
    selectedArea: string;
    setSelectedArea: React.Dispatch<React.SetStateAction<string>>;
}

function FilterComponent({ selectedLocal, setSelectedLocal, selectedArea, setSelectedArea }: FilterComponentProps) {
    return (
        <div className="content-filters">
            <p className="content-filters-text">Filtrar por: </p>
            <span className="content-filters-box">
                <Combobox
                    options={options_local}
                    selectedValue={selectedLocal}
                    onValueChange={setSelectedLocal}
                    placeholder="Localização"
                />
                <Combobox
                    options={options_area}
                    selectedValue={selectedArea}
                    onValueChange={setSelectedArea}
                    placeholder="Área do Conhecimento"
                />
            </span>
        </div>
    );
}

function Filtros({ filtroPalavrasChave }: { filtroPalavrasChave?: string[] }) {
    const [selectedLocal, setSelectedLocal] = React.useState<string>("");
    const [selectedArea, setSelectedArea] = React.useState<string>("");

    return (
        <div className="filters">
            <FilterComponent
                selectedLocal={selectedLocal}
                setSelectedLocal={setSelectedLocal}
                selectedArea={selectedArea}
                setSelectedArea={setSelectedArea}
            />
            <ListInstitutoCards filter={{ local: selectedLocal, area: selectedArea, palavras_chave: filtroPalavrasChave }} />
        </div>
    );
}

export default Filtros;