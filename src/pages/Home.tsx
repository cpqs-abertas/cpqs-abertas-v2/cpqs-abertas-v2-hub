import "../styles/globals.css"
import "./Home.css"
import ListInstitutoCards from "@/components/ListInstitutoCards/ListInstitutoCards"


function Home() {
  return (
    <div className="content">
      <div className="content-text">
        {/* <p className="content-lema">Pela democratização da informação acadêmica!</p> */}
        <div className="content-apresentacao">
          <strong>USP Aberta</strong> veio para democratizar a informação acadêmica, presente  no currículo Lattes.
          Conheça os docentes e unidades da <strong>Universidade de São Paulo</strong>.
          Estamos no processo de coleta de dados, portanto há unidades ainda não cadastradas nesta plataforma.
        </div>
      </div>
      <ListInstitutoCards />
    </div>
  )
}

export default Home