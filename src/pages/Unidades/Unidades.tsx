import "./Unidades.css";

import Filtros from "@/components/Filtros/Filtros";

function Unidades() {
    return (
        <div className="content-unidades">
            <div className="content-head">
                <h1 className="content-title">UNIDADES</h1>
            </div>
            <Filtros />
        </div>
    );
}

export default Unidades;