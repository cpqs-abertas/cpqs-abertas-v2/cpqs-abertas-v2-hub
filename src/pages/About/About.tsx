import CoordinationWrapper from "../../components/wrappers/CoordinationWrapper";
import { COORDENACAO, COORDENACAO_DESIGN } from "../../data";
import USPFoto from "../../assets/imagens/image 24.png";
import ImgHeader414w from "../../assets/imagens/image_header_414w.png";
import ImgHeader744w from "../../assets/imagens/image_header_744w.png";
import Team from "../../components/Team/Team";
import "./About.css";

function About() {
    return (
        <div className="c-about">
                <h1>
                        <img 
                                className="c-about__uspimg"
                                srcSet={
                                        `
                                                ${ImgHeader414w} 414w, 
                                                ${ImgHeader744w} 744w
                                        `
                                }
                                sizes={"(max-width: 414px) 414px, (max-width: 744px) 744px"}
                                src={USPFoto} alt="foto-usp" 
                        />
                </h1>
            <span id="c-about__text">
                <p>
                    A <strong>USP Aberta</strong> é um projeto promovido pela Pró-Reitoria de Pesquisa e Inovação da Universidade (PRPI) com o intuito de dar <strong>visibilidade
                    à produção intelectual da Universidade de São Paulo</strong>, difundindo sua especificidade e diversidade através de dados extraídos do currículo Lattes dos docentes.
                </p>

                <p>
                    É <strong>compromisso das universidades públicas ampliar o acesso de suas pesquisas acadêmicas</strong> e seus resultados (produções bibliográficas, técnicas e artísticas),
                    permitindo quantificá-los e qualificá-los em termos de impacto social, impacto econômico, inovação tecnológica,desdobramentos em políticas públicas e de sustentabilidade.
                </p>

                <p>
                    O projeto já passou por diversas etapas, contando com a participação de docentes e discentes da <strong>Faculdade de Arquitetura e Urbanismo e  Design</strong> (FAUD) e do <strong>Instituto de Matemática e Estatística</strong> (IME), todos devidamente creditatos abaixo. Temos o apoio da Superintendência de Tecnologia da Informação (STI), da Comissão de pesquisa e Inovação CPqI e da direção das unidades. 
                </p>

            </span>
            <section id="c-about__photos">
                <div>
                        <h2>Coordenação Geral</h2>
                        <ul>{COORDENACAO.map((person) =>(
                                <CoordinationWrapper key={person.title} {...person} />
                                ))}
                        </ul>

                </div>
                <div>
                                <h2>Coordenação de Design</h2>
                                <ul>{COORDENACAO_DESIGN.map((person) =>(
                                        <CoordinationWrapper key={person.title} {...person} />
                                        ))}
                                </ul>
                </div>
            </section>
            <section id="c-about__team-banner">
                <h2>Equipe</h2>
            </section>
            <Team year="2019" />
            <Team year="2020" />
            <Team year="2021" />
            <Team year="2022" />
            <Team year="2023" />
            <Team year="2024" />
        </div>
    );
}

export default About;