import React from 'react';
import { useLocation } from 'react-router-dom';
import { InstitutoCardsProps } from '@/components/InstitutoCards/InstitutoCards';
import "./Resultados.css";
import Filtros from '@/components/Filtros/Filtros';
interface ResultadosProps {
    results: InstitutoCardsProps[];
}

const Resultados: React.FC<ResultadosProps> = ({ }) => {
    const location = useLocation();
    const searchParams = new URLSearchParams(location.search);
    const searchTerm = searchParams.get('search') || '';

    return (
        <div className="resultados-container">
            <h1 className='resultados-text'>Resultados da Pesquisa de "{searchTerm}":</h1>
            <Filtros filtroPalavrasChave={[searchTerm]} />
        </div>
    );
};

export default Resultados;
