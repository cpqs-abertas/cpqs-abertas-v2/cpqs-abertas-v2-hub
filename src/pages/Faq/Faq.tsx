import "./Faq.css";
import FaqWrapper from "./FaqWrapper/FaqWrapper";
import { DUVIDAS } from "@/data";

function Faq() {
        
        return (
                <div className={"c-faq"}>
                        <div className={"c-faq__title"}>
                                <h1 className={"c-faq__title-h1"}>
                                        <strong>
                                                Perguntas Frequentes
                                        </strong>
                                </h1>
                        </div>
                        <ol>
                                {DUVIDAS.map((duvida) => (
                                        <FaqWrapper key={duvida.number} {...duvida} />
                                ))}
                        </ol>
                </div>

        );
}

export default Faq;