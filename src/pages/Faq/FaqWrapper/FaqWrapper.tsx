import { useState } from "react";
import "./FaqWrapper.css";
import ExpandButton from "@/assets/botoes/botao.svg";
interface FaqWrapperProps {
        number: string;
        question: string;
        answer: string;
}

function FaqWrapper( { number, question, answer } : FaqWrapperProps ) {

        const [buttonClicked, setButtonClicked] = useState(false);

        const handleClick = () => {
                setButtonClicked(!buttonClicked);
        };

        return (
                <li className={"c-faq__aqwrap"}>
                        <div className={`c-faq__question ${buttonClicked ? "":"closed"}`}>
                                <p id={"c-faq__question-p"}>
                                        <strong>{number}</strong>
                                </p>

                                <h2 id={"c-faq__question-h2"}>
                                        <strong>
                                                {question}
                                        </strong>
                                </h2>

                                <button onClick={handleClick} className={`c-faq__answerbutton ${buttonClicked ? "up" : ""}` }>
                                        <img src={ExpandButton} />
                                </button>

                        </div>
                        <div className="c-faq__answer-centered">
                                {buttonClicked && <p id={"c-faq__answer-p"}>{answer}</p>}
                        </div>
                </li>

        );
}

export default FaqWrapper;