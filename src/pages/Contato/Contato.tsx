import "./Contato.css"
import React, { useState } from "react";

function Contato() {
        //const localDns = import.meta.env.VITE_LOCAL_DNS;
        const publicDns = import.meta.env.VITE_PUBLIC_DNS;

        const [formData, setFormData] = useState( {
                name: "",
                email: "",
                message: "",
        } );
        const [responseMessage, setResponseMessage] = useState("");

        function handleInputChange(e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {
                setFormData( {...formData, [e.target.name]: e.target.value} ); 
        }

        async function handleSendMessage() {
                if(!formData.name || !formData.email || !formData.message) {
                        setResponseMessage("Todos os campos são obrigatórios");
                        return;
                }

                try {

                        const response = await fetch(`${publicDns}`, {
                                method: "POST",
                                headers: {
                                        "Content-Type": "application/json",
                                },
                                body: JSON.stringify(formData),
                        });

                        const data = await response.json();

                        if(response.status === 200) {// Um popup ou renderizar na propria pagina ?
                                //alert("Email enviado com sucesso !");
                                setResponseMessage("Email enviado com sucesso !");
                        } else {
                                setResponseMessage(`Falha no envio: ${data.message}`);
                        }

                } catch(error) {
                        console.error("Error: ", error);
                        alert("Erro ao tentar efetuar o envio");
                }

        }

        return (
                <div className={"c-contato"}>

                        <form className={"c-contato__input"} onSubmit={(e) => e.preventDefault}>

                                <div className={"c-contato__inputname"}>
                                        <input 
                                                placeholder="Nome" 
                                                autoComplete="name" 
                                                name="name" 
                                                type="text"
                                                value={formData.name}
                                                onChange={handleInputChange}
                                        />
                                </div>

                                <div className={"c-contato__inputemail"}>
                                        <input 
                                                placeholder="E-mail" 
                                                required 
                                                autoComplete="email" 
                                                name="email"
                                                type="email"
                                                value={formData.email}
                                                onChange={handleInputChange}
                                        />
                                </div>

                                <div className={"c-contato__inputmsg"}>
                                        <textarea 
                                                placeholder="Mensagem" 
                                                required 
                                                name="message"
                                                value={formData.message}
                                                onChange={handleInputChange}
                                        />
                                </div>

                        </form>

                        <div className={"send-button"}>
                                <button type={"button"} onClick={handleSendMessage}>Enviar</button>
                        </div>
                        {responseMessage && <p>{responseMessage}</p>}

                </div>
        );
}

export default Contato;